const axios  = require("axios");
const { parse, format } = require('date-fns');


axios.get('https://dpg.gg/test/calendar.json')
    .then(function (response) {
        // handle success
        let dates = response.data;
        const entries = Object.entries(dates);

        // console.log(dates);
        for (const [key, value] of dates) {
            console.log(`${key}: ${value}`, '1');
        }
    })
    .catch(function (error) {
        // handle error
        console.log(error);
    })
    .finally(function () {
        // always executed
    });

// Parse the input date string
const inputDate = parse("2023-04-24", "yyyy-MM-dd", new Date());

// Format the parsed date according to the desired format
const formattedDate = format(inputDate, "EEEE, MMMM d, yyyy");

// console.log(formattedDate);